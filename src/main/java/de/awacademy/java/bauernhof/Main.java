package de.awacademy.java.bauernhof;

public class Main {

    public static void main(String[] args) {

        Bauernhof bauernhof = new Bauernhof();

        /*
         * Alle Tiere geben ihren Laut.
         */
        bauernhof.gibLautHund();
        bauernhof.gibLautKatze();
        bauernhof.gibLautMaus();
        bauernhof.gibLautRegenwurm();
        bauernhof.gibLautKuh();

        /*
         * Die Katze soll nun den Bauernhof bewachen und
         * sich melden, sobald jemand Unbefugtes den Hof
         * betritt.
         */
        Bauernhof bewachterBauernhof = new Bauernhof("Katze");
        // Nun kommt ein Unbefugter auf den Hof.
        System.out.println("Ein Unbefugter betritt den Hof.");
        bewachterBauernhof.melden();

        /*
         * Die Katze taugt recht wenig als Bewacher für den
         * Bauernhof und nun soll der Hund die Wache übernehmen.
         */
        bewachterBauernhof.setBewacher("Hund");
        System.out.println("Nun kommt noch ein Unbefugter auf den Hof.");
        bewachterBauernhof.melden();

        /*
         * Jetzt werden Ziegen für den Bauernhof angeschafft.
         * Was ist im Code zu tun, um die neuen Tiere ebenfalls
         * für die Hofwache abzustellen?
         */
        // TODO Implementiere die Lösung im Bauerhof
        // TODO und einem cleveren Bauernhof mit Interfaces.

    }
}
