package de.awacademy.java.bauernhof;

/**
 * Ein Bauernhof hat Hunde, Katzen, Mäuse, Regenwürmer und Kühe.
 */
public class Bauernhof {

    /**
     * Der Bewacher kann "Hund", "Katze", "Maus",
     * "Regenwurm" sein. Eine Kuh kann keine Wache
     * übernehmen.
     */
    private String bewacher = null;

    /**
     * Bauernhof anlegen.
     */
    public Bauernhof() {
    }

    /**
     * Bauernhof mit Bewacher anlegen.
     * @param bewacher
     */
    public Bauernhof(String bewacher) {
        this.bewacher = bewacher;
    }

    /**
     * Der Bewacher meldet das Eindringen eines Unbefugten
     * auf den Bauernhof durch seinen eigenen Laut.
     */
    public void melden() {
        if("Hund".equals(this.bewacher)) {
            gibLautHund();
        } else if("Katze".equals(this.bewacher)) {
            gibLautKatze();
        } else if("Maus".equals(this.bewacher)) {
            gibLautMaus();
        } else if("Regenwurm".equals(this.bewacher)) {
            gibLautKatze();
        } else if("Kuh".equals(this.bewacher)) {
            System.out.println("Eine Kuh ist ungeeignet, den Hof zu bewachen");
        } else {
            System.out.println("Der Hof ist unbewacht.");
        }

    }

    /**
     * Der Laut des Hundes.
     */
    public void gibLautHund() {
        String laut = "Wau wau";
        System.out.println(laut);
    }

    /**
     * Der Laut der Katze.
     */
    public void gibLautKatze() {
        String laut = "Miau";
        System.out.println(laut);
    }

    /**
     * Der Laut der Maus.
     */
    public void gibLautMaus() {
        String laut = "Fiep fiep";
        System.out.println(laut);
    }

    /**
     * Der Laut des Regenwurms.
     */
    public void gibLautRegenwurm() {
        String laut = "Zzzz";
        System.out.println(laut);
    }

    /**
     * Der Laut der Kuh.
     */
    public void gibLautKuh() {
        String laut = "Muuuh";
        System.out.println(laut);
    }

    /** Bewacher austauschen */
    public void setBewacher(String bewacher) {
        this.bewacher = bewacher;
    }

}
