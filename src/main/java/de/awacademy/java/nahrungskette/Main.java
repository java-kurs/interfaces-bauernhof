package de.awacademy.java.nahrungskette;

public class Main {

    public static void main(String[] args) {

        int a = 1308;
        int b = 2616;

        boolean kleiner = a < b;
        boolean gleich = a == b;
        boolean groesser = a > b;

        System.out.println("a kleiner b: " + kleiner);
        System.out.println("a gleich  b: " + gleich);
        System.out.println("a größer  b: " + groesser);
    }
}
