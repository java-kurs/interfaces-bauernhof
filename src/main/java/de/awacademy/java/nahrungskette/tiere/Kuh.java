package de.awacademy.java.nahrungskette.tiere;

public class Kuh extends AbstraktesTier {

    /**
     * Die Kuh steht auf einer Stufe mit
     * dem Regenwurm.
     */
    public Kuh() {
        ordnung = 10;
    }

    @Override
    public void gibLaut() {
        String laut = "Muuuh";
        System.out.println(laut);
    }

    /**
     * Überschreibe das Fessen für die Kuh, da sie zusätzlich
     * noch widerkäut. Das normale Fressen wird von der
     * abstrakten Klasse übernommen (super.fressen()).
     */
    @Override
    public void fressen() {
        super.fressen();
        System.out.println("Widerkäuen und schlucken.");
    }

}
