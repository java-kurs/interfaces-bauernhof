package de.awacademy.java.nahrungskette.tiere;

import java.util.concurrent.AbstractExecutorService;

public abstract class AbstraktesTier implements TierInterface, Comparable<AbstraktesTier> {

    /**
     * Ordnung in der Nahrungskette wird für alle deklariert.
     * Festgelegt wird der Wert in den abgeleiteten Klassen.
     */
    protected int ordnung;

    /**
     * Abstrakte Methode wird von jedem Tier selbst implementiert,
     * da die Laute der Tiere unterschiedlich sind.
     */
    public abstract void gibLaut();

    /**
     * Das Fressen wird 1x für alle Tiere implementiert, da diese
     * mindestens beißen, kauen und schlucken.
     */
    @Override
    public void fressen() {
        System.out.println("Beißen, kauen und schlucken.");
    }

    /**
     * Die abstrakte Klasse stellt den Vergleich für alle
     * abgeleiteten Tiere zur Verfügung.
     * @param anderesTier
     * @return
     */
    @Override
    public int compareTo(AbstraktesTier anderesTier) {
        if(this.ordnung < anderesTier.getOrdnung()) {
            return -1;
        } else if(this.ordnung == anderesTier.getOrdnung()) {
            return 0;
        } else {
            // kann nur noch größer sein
            return 1;
        }
    }

    public int getOrdnung() {
        return ordnung;
    }
}
