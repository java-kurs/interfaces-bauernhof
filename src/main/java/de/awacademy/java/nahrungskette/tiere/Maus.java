package de.awacademy.java.nahrungskette.tiere;

public class Maus extends AbstraktesTier {

    public Maus() {
        ordnung = 20;
    }

    @Override
    public void gibLaut() {
        String laut = "Fiep fiep";
        System.out.println(laut);
    }

}
