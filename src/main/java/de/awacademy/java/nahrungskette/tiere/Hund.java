package de.awacademy.java.nahrungskette.tiere;

public class Hund extends AbstraktesTier {

    /**
     * Der Konstruktor bestimmt die Stellung in
     * der Nahrungskette.
     */
    public Hund() {
        ordnung = 40;
    }

    @Override
    public void gibLaut() {
        String laut = "Wau wau";
        System.out.println(laut);
    }

}
