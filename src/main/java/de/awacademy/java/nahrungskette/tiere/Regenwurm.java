package de.awacademy.java.nahrungskette.tiere;

public class Regenwurm extends AbstraktesTier {

    public Regenwurm() {
        ordnung = 10;
    }

    @Override
    public void gibLaut() {
        String laut = "Zzzz";
        System.out.println(laut);
    }

}
