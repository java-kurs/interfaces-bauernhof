package de.awacademy.java.nahrungskette.tiere;

public interface TierInterface {

    void gibLaut();

    void fressen();

}
