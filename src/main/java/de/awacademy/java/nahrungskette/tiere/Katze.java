package de.awacademy.java.nahrungskette.tiere;

public class Katze extends AbstraktesTier {

    public Katze() {
        ordnung = 30;
    }

    @Override
    public void gibLaut() {
        String laut = "Miau";
        System.out.println(laut);
    }

}
