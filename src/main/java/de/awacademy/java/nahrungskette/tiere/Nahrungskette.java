package de.awacademy.java.nahrungskette.tiere;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Nahrungskette {

    public static void main(String[] args) {

        /**
         * Unsortierte Liste aller Tiere.
         */
        List<AbstraktesTier> tiere = new ArrayList<>(Arrays.asList(
                new Maus(), new Regenwurm(), new Katze(), new Hund()));

        System.out.println(tiere);

        /*
         * Diese Methode nimmt Objekte abgeleitet von Comparable
         * und führt die Sortierung innerhalb der Liste durch.
         */
        Collections.sort(tiere);

        System.out.println(tiere);
    }
}
