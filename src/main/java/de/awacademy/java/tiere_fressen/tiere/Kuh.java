package de.awacademy.java.tiere_fressen.tiere;

public class Kuh extends AbstraktesTier {

    @Override
    public void gibLaut() {
        String laut = "Muuuh";
        System.out.println(laut);
    }

    /**
     * Überschreibe das Fessen für die Kuh, da sie zusätzlich
     * noch widerkäut. Das normale Fressen wird von der
     * abstrakten Klasse übernommen (super.fressen()).
     */
    @Override
    public void fressen() {
        super.fressen();
        System.out.println("Widerkäuen und schlucken.");
    }

}
