package de.awacademy.java.tiere_fressen.tiere;

public interface TierInterface {

    void gibLaut();

    void fressen();

}
