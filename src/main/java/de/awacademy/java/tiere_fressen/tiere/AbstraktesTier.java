package de.awacademy.java.tiere_fressen.tiere;

public abstract class AbstraktesTier implements TierInterface {

    /**
     * Abstrakte Methode wird von jedem Tier selbst implementiert,
     * da die Laute der Tiere unterschiedlich sind.
     */
    public abstract void gibLaut();

    /**
     * Das Fressen wird 1x für alle Tiere implementiert, da diese
     * mindestens beißen, kauen und schlucken.
     */
    @Override
    public void fressen() {
        System.out.println("Beißen, kauen und schlucken.");
    }
}
