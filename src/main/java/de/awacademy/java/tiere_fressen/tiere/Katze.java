package de.awacademy.java.tiere_fressen.tiere;

public class Katze extends AbstraktesTier {

    @Override
    public void gibLaut() {
        String laut = "Miau";
        System.out.println(laut);
    }

}
