package de.awacademy.java.tiere_fressen;

import de.awacademy.java.tiere_fressen.tiere.*;

public class Main {

    public static void main(String[] args) {

        /**
         * Bestimme die Tiere des cleveren Bauernhofs.
         */
        TierischerBauernhof bauernhof = new TierischerBauernhof();
        bauernhof.getTiere().add(new Hund());
        bauernhof.getTiere().add(new Katze());
        bauernhof.getTiere().add(new Maus());
        bauernhof.getTiere().add(new Regenwurm());
        bauernhof.getTiere().add(new Kuh());

        /*
         * Alle Tiere geben ihren Laut.
         */
        for(TierInterface tier : bauernhof.getTiere()) {
            tier.gibLaut();
        }

        /*
         * Es ist Futterzeit und alle Tiere fressen.
         */
        for(TierInterface tier : bauernhof.getTiere()) {
            tier.fressen();
        }

        /*
         * Die Katze soll nun den Bauernhof bewachen und
         * sich melden, sobald jemand Unbefugtes den Hof
         * betritt.
         */
        TierischerBauernhof bewachterBauernhof = new TierischerBauernhof(new Katze());
        // Nun kommt ein Unbefugter auf den Hof.
        System.out.println("Ein Unbefugter betritt den Hof.");
        bewachterBauernhof.melden();

        /*
         * Die Katze taugt recht wenig als Bewacher für den
         * Bauernhof und nun soll der Hund die Wache übernehmen.
         */
        bewachterBauernhof.setBewacher(new Hund());
        System.out.println("Nun kommt noch ein Unbefugter auf den Hof.");
        bewachterBauernhof.melden();

        /*
         * Jetzt werden Ziegen für den Bauernhof angeschafft.
         * Was ist im Code zu tun, um die neuen Tiere ebenfalls
         * für die Hofwache abzustellen?
         */
        // TODO Implementiere die Lösung im Bauerhof
        // TODO und einem cleveren Bauernhof mit Interfaces.

    }
}
