package de.awacademy.java.cleverer_bauernhof;

import de.awacademy.java.cleverer_bauernhof.tiere.TierInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Ein Bauernhof hat Hunde, Katzen, Mäuse, Regenwürmer und Kühe.
 */
public class ClevererBauernhof {

    /**
     * Alle Tiere des Bauernhofs.
     */
    private List<TierInterface> tiere = new ArrayList<>();

    /**
     * Der Bewacher kann "Hund", "Katze", "Maus",
     * "Regenwurm" sein. Eine Kuh kann keine Wache
     * übernehmen.
     */
    private TierInterface bewacher = null;

    /**
     * Bauernhof anlegen.
     */
    public ClevererBauernhof() {
    }

    /**
     * Bauernhof mit Bewacher anlegen.
     * @param bewacher
     */
    public ClevererBauernhof(TierInterface bewacher) {
        this.bewacher = bewacher;
    }

    /**
     * Der Bewacher meldet das Eindringen eines Unbefugten
     * auf den Bauernhof durch seinen eigenen Laut.
     */
    public void melden() {
        bewacher.gibLaut();
    }

    public List<TierInterface> getTiere() {
        return tiere;
    }

    /** Bewacher austauschen */
    public void setBewacher(TierInterface bewacher) {
        this.bewacher = bewacher;
    }

}
