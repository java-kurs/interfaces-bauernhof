package de.awacademy.java.cleverer_bauernhof.tiere;

public class Kuh implements TierInterface {

    @Override
    public void gibLaut() {
        String laut = "Muuuh";
        System.out.println(laut);
    }

}
