package de.awacademy.java.cleverer_bauernhof.tiere;

public class Katze implements TierInterface {

    @Override
    public void gibLaut() {
        String laut = "Miau";
        System.out.println(laut);
    }

}
